# hd-gulp
Opinionated [Gulp](https://gulpjs.com/) tasks for various projects

#### Tasks:
* clean - _Remove given paths_
* copy - _Move files from one location to another_
* imagemin - _Copy optimized images to another location_
* lint - _Lint JS files using [standard](https://standardjs.com/) code style_
* scripts - _Compile ES6+ JS files into format that is supported by browsers (ES5)_
* ~~stylelint - _Lint CSS files_~~
* styles - _Compile Stylus files into CSS_
* templates - _Build PUG template files_

## Installation
**hd-gulp** is referenced in project's `package.json` using its git repository url in following format:
```sh
git+ssh://git@bitbucket.org:hyperdigital/hd-gulp.git#<branch/commit/tag>
```
It is recommended to use **tags** as they should not break your builds if you follow [Semantic versioning](https://semver.org/).

## Usage & Configuration

Example **`package.json`**:
```json
{
    "scripts": {
        "watch": "gulp",
        "build": "gulp build"
    },
    "devDependencies": {
        "gulp": "^4.0.0",
        "hd-gulp": "git+ssh://git@bitbucket.org:hyperdigital/hd-gulp.git#1.0.0"
    }
}
```

Example **`gulpfile.js`**:
```js
const gulp = require('gulp')
const tasks = require('hd-gulp')

const gs = gulp.series
const gp = gulp.parallel

const STYLES = {
    input: './src/styles/index.styl',
    output: './dist/main.bundle.css'
}
const SCRIPTS = {
    input: './src/scripts/index.js',
    output: './dist/main.bundle.js',
    options: {
        format: 'iife',
        external: ['jquery'],
        globals: { jquery: 'jQuery' },
    }
}

gulp.task('lint:js', () => tasks.lint('./src/scripts/**/*.js'))
gulp.task('build:js', () => tasks.scripts(SCRIPTS.input, SCRIPTS.output, SCRIPTS.options))
gulp.task('build:css', () => tasks.styles(STYLES.input, STYLES.output))
gulp.task('build', gp('build:js', 'build:css'))

gulp.task('default', gp('build'), () => {
    gulp.watch('./src/scripts/**', gs('lint:js', 'build:js'))
})
```

Terminal usage:
```sh
# production
npm run build
```
```sh
# development
npm run watch
```

For more examples take a look inside `tasks` folder.


## Recipes

### Clean task
```js
const CLEAN_FILES = ['dist/*']
gulp.task('clean', () => tasks.clean(CLEAN_FILES))
```

### Copy task
```js
const COPY_FILES = ['node_modules/jquery/dist/jquery.min.js',]
gulp.task('copy', () => tasks.copy(COPY_FILES, './dist/vendor'))
```

### Imagemin task
```js
const IMAGE_FILES = ['./src/images/**/*']
gulp.task('imagemin', () => tasks.imagemin(IMAGE_FILES, './dist/images'))
```

### Lint task
```js
const LINT_FILES = ['./src/scripts/**/*.js']
gulp.task('lint', () => tasks.lint(LINT_FILES))
```

### Scripts task
```js
const SCRIPTS_CFG = {
    input: './src/scripts/index.js',
    output: './dist/main.bundle.js',
}
gulp.task('scripts', () => tasks.scripts(SCRIPTS_CFG.input, SCRIPTS_CFG.output))
```

### Stylelint task
```js
const STYLELINT_FILES = ['./src/styles/**/*.css']
gulp.task('stylelint', () => tasks.stylelint(STYLELINT_FILES))
```

### Styles task
```js
const STYLES_CFG = {
    input: './src/styles/index.styl',
    output: './dist/main.bundle.css',
}
gulp.task('styles', () => tasks.styles(STYLES_CFG.input, STYLES_CFG.output))
```

### Templates task
```js
const TEMPLATES_CFG = {
    input: ['./src/templates/pages/*.pug'],
    output: './dist',
    options: {
        pretty: false
    }
}
gulp.task('templates', () => tasks.templates(TEMPLATES_CFG.input, TEMPLATES_CFG.output, TEMPLATES_CFG.options))
```
