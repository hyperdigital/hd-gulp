const shell = require('gulp-shell')

function stylint (input, options = '-c .stylintrc') {
  return shell.task([`stylint ${input} ${options}`])
}

module.exports = stylint
