const gulp = require('gulp')
const gulpLoadPlugins = require('gulp-load-plugins')

const TASK_TITLE = 'COPY:'
const g = gulpLoadPlugins()

/**
 * Copy files to destination
 *
 * @see https://github.com/gulpjs/gulp/blob/master/docs/API.md#gulpsrcglobs-options
 *
 * @param input {String[]|String} Source files
 * @param output {String} Target folder
 * @param options {Object} `gulp.src` options
 */
function copy (input, output, options = {}) {
  return gulp.src(input, options)
    .pipe(g.changed(output))
    .pipe(gulp.dest(output))
    .pipe(g.size({ title: TASK_TITLE }))
}

module.exports = copy
