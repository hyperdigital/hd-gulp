const path = require('path')
const extend = require('deep-extend')
const gulp = require('gulp')
const gulpLoadPlugins = require('gulp-load-plugins')
const rollup = require('rollup')
const babel = require('rollup-plugin-babel')
const json = require('rollup-plugin-json')

const TASK_TITLE = 'SCRIPTS:'
const g = gulpLoadPlugins()
const defaults = {
  format: 'iife',
  name: undefined,
  external: ['jquery'],
  globals: { jquery: 'jQuery' },
}

/**
 * Compile and concatenate JS files using Rollup + Babel.
 *
 * @see https://rollupjs.org/
 *
 * @param input {String[]|String} Source file
 * @param output {String} Target file
 * @param options {Object} `rollup` configuration
 * @param options.format {String} The format of the generated bundle: `amd, cjs, esm, iife, umd, system`
 * @param options.name {String} The variable name, representing your iife/umd bundle, by which other scripts on the same page can access it
 * @param options.external {String[]} External libraries
 * @param options.globals {Object} Global libraires
 */
function scripts (input, output, options = {}) {
  const settings = extend(defaults, options)

  const outputFile = path.basename(output)
  const outputFolder = path.dirname(output)

  return gulp.src(input)
    .pipe(g.plumber())
    .pipe(g.sourcemaps.init({ loadMaps: true }))
    .pipe(g.betterRollup({
      rollup: rollup,
      plugins: [babel(), json()],
      external: settings.external
    }, {
      file: outputFile,
      name: settings.name,
      format: settings.format,
      globals: settings.globals
    }))
    .pipe(g.sourcemaps.write())
    .pipe(gulp.dest(outputFolder))
    .pipe(g.terser())
    .pipe(g.rename({ suffix: '.min' }))
    .pipe(g.sourcemaps.write('.'))
    .pipe(gulp.dest(outputFolder))
    .pipe(g.size({ title: TASK_TITLE }))
}

module.exports = scripts
