const gulp = require('gulp')
const gulpLoadPlugins = require('gulp-load-plugins')

const TASK_TITLE = 'IMAGES:'
const g = gulpLoadPlugins()

/**
 * Optimize images
 *
 * @param input {String[]|String} Source files
 * @param output {String} Target folder
 * @param options {Object} `gulp.src` options
 */
function imagemin (input, output, options = {}) {
  return gulp.src(input, options)
    .pipe(g.plumber())
    .pipe(g.changed(output))
    .pipe(g.imagemin())
    .pipe(gulp.dest(output))
    .pipe(g.size({ title: TASK_TITLE }))
}

module.exports = imagemin
