const extend = require('deep-extend')
const gulp = require('gulp')
const gulpLoadPlugins = require('gulp-load-plugins')

const TASK_TITLE = 'TEMPLATES:'
const g = gulpLoadPlugins()

const gulpPugDefaults = {
  pretty: true
}

/**
 * Build PUG templates
 *
 * @see https://pugjs.org/
 * @see https://www.npmjs.com/package/gulp-pug
 *
 * @param input {String[]|String} Source files
 * @param output {String} Target folder
 * @param options {Object} `gulp-pug` configuration
 */
function templates (input, output, options = {}) {
  const settings = extend(gulpPugDefaults, options)

  return gulp.src(input)
    .pipe(g.plumber())
    .pipe(g.pug(settings))
    .pipe(gulp.dest(output))
    .pipe(g.size({ title: TASK_TITLE }))
}

module.exports = templates
