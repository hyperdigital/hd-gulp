const del = require('del')
const log = require('fancy-log')

/**
 * Clean given folders/files
 *
 * @param input {[String]} Paths to remove
 */
function clean (input) {
  input.forEach((item) => log.info('-' + item))
  return del(input)
}

module.exports = clean
