const gulp = require('gulp')
const gulpLoadPlugins = require('gulp-load-plugins')

const TASK_TITLE = 'SVG:'
const g = gulpLoadPlugins()

/**
 * Generate SVG sprite
 *
 * @param input {[String]|String} Source files
 * @param output {String} Target folder
 * @param options {Object} `gulp.src` options
 */
function svg (input, output, options = {}) {
  return gulp.src(input, options)
    .pipe(g.plumber())
    .pipe(g.svgmin({
      plugins: [{
        removeStyleElement: true
      }, {
        removeAttrs: {
          attrs: ['fill', 'stroke', 'fill.*', 'stroke.*']
        }
      }]
    }))
    .pipe(g.svgstore({ inlineSvg: true }))
    .pipe(gulp.dest(output))
    .pipe(g.size({ title: TASK_TITLE }))
}

module.exports = svg
