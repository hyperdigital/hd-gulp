const extend = require('deep-extend')
const gulp = require('gulp')
const plumber = require('gulp-plumber')
const standard = require('gulp-standard')

const gulpStandardDefaults = {
  reporter: 'default',
  options: {
    breakOnError: false,
    breakOnWarning: false,
    quiet: true,
    showRuleNames: false,
    showFilePath: false,
  }
}

/**
 * Lint JS files using Standard code style
 *
 * @see https://standardjs.com/
 * @see https://www.npmjs.com/package/gulp-standard
 *
 * @param input {String[]|String} Source files
 * @param options {Object} `gulp-standard` configuration
 * @param options.reporter {String} Reporter
 * @param options.options {Object} Reporter options
 * @param options.options.breakOnError {Boolean} Emit gulp error on reported error
 * @param options.options.breakOnWarning {Boolean} Emit gulp error on reported warning
 * @param options.options.quiet {Boolean} Suppress success messages, only show errors
 * @param options.options.showRuleNames {Boolean} Show rule names for errors/warnings (to ignore specific rules)
 * @param options.options.showFilePath {Boolean} Show the full file path with the line and column numbers
 */
function lint (input, options = {}) {
  const settings = extend(gulpStandardDefaults, options)

  return gulp.src(input)
    .pipe(plumber())
    .pipe(standard())
    .pipe(standard.reporter(settings.reporter, settings.options))
}

module.exports = lint
