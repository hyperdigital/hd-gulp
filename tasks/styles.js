const path = require('path')
const gulp = require('gulp')
const gulpLoadPlugins = require('gulp-load-plugins')
const lost = require('lost')
const presetEnv = require('postcss-preset-env')
const pxtorem = require('postcss-pxtorem')
const emMediaQuery = require('postcss-em-media-query')
const cssmqpacker = require('css-mqpacker')
const cssnano = require('cssnano')

const TASK_TITLE = 'STYLES:'
const g = gulpLoadPlugins()

/**
 * Build CSS from Stylus files with help of PostCSS.
 *
 * @param input {String[]|String} Source file
 * @param output {String} Target file
 * @param rootValue {Number} The root element font size, 0 disables transformation
 */
function styles (input, output, rootValue = 16) {
  const outputFile = path.basename(output)
  const outputFolder = path.dirname(output)
  const postcssPlugins = [
    lost(),
    presetEnv(),
    emMediaQuery()
  ]

  if (rootValue) postcssPlugins.push(pxtorem({ rootValue }))

  return gulp.src(input)
    .pipe(g.plumber())
    .pipe(g.sourcemaps.init({ loadMaps: true }))
    .pipe(g.stylus({
      'use': [],
      'url': 'embed-url',
      'include css': true
    }))
    .pipe(g.postcss(postcssPlugins))
    .pipe(g.rename(outputFile))
    .pipe(g.sourcemaps.write())
    .pipe(gulp.dest(outputFolder))
    .pipe(g.postcss([
      cssmqpacker({ sort: true }),
      cssnano({ reduceIdents: false })
    ]))
    .pipe(g.rename({ suffix: '.min' }))
    .pipe(g.sourcemaps.write('.'))
    .pipe(gulp.dest(outputFolder))
    .pipe(g.size({ title: TASK_TITLE }))
}

module.exports = styles
