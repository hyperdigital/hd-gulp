const svg = require('./tasks/svg.js')
const lint = require('./tasks/lint.js')
const copy = require('./tasks/copy.js')
const clean = require('./tasks/clean.js')
const styles = require('./tasks/styles.js')
const stylint = require('./tasks/stylint.js')
const scripts = require('./tasks/scripts.js')
const imagemin = require('./tasks/imagemin.js')
const templates = require('./tasks/templates.js')

module.exports = {
  svg: svg,
  lint: lint,
  copy: copy,
  clean: clean,
  styles: styles,
  stylint: stylint,
  scripts: scripts,
  imagemin: imagemin,
  templates: templates
}
